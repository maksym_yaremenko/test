import type { User } from '@/types'
import { create } from 'zustand'

type CandidatesStoreType = {
  candidates: User[]
  setCandidates: (candidates: User[] | ((prev: User[]) => User[])) => void
}

export const useCandidates = create<CandidatesStoreType>((set) => ({
  candidates: [],
  setCandidates(candidates) {
    if (typeof candidates === 'function') {
      set((state) => ({ candidates: candidates(state.candidates) }))
    } else {
      set({ candidates })
    }
  },
}))
