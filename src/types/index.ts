export type User = {
  firstName: string;
  lastName: string;
  location: string;
  workHistory: {
      company: string;
      title: string;
      startDate: string;
      endDate?: string;
  }[];
}
