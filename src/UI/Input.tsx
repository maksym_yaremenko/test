import StarsIcon from "@/assets/stars.svg";
import Image from "next/image";
import { ElementRef, RefObject } from "react";

type InputProps = {
  value: string;
  onChange: (value: string) => void;
  placeholder?: string;
  inputRef?: RefObject<ElementRef<"input">>;
  label?: string;
};

export const Input = ({
  value,
  onChange,
  placeholder,
  label,
  inputRef,
}: InputProps) => (
  <label className="flex flex-col gap-1">
    {label && <p className="text-gray-900 text-xs font-medium">{label}</p>}

    <div className="flex gap-1.5 items-center p-1.5 border border-gray-300 rounded transition-colors duration-300 focus-within:border-gray-500">
      <i className="p-[5px] rounded bg-gray-200">
        <Image src={StarsIcon} alt="Stars" />
      </i>

      <input
        placeholder={placeholder}
        value={value}
        onChange={(event) => onChange(event.target.value)}
        ref={inputRef}
        className="focus-visible:outline-none placeholder:text-gray-500 w-full"
      />
    </div>
  </label>
);
