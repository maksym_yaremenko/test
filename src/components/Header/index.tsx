import WorkHQIcon from "@/assets/work_hq.svg";
import Image from "next/image";

export const Header = () => {
  return (
    <header className="py-[13px] px-5">
      <Image src={WorkHQIcon} alt="WorkHQ Icon" />
    </header>
  );
};
