import { Input } from "@/UI/Input";
import { fetchUsersData } from "@/api/getUsers";
import { debounce } from "@/helpers/debounce";
import { useCandidates } from "@/store";
import type { User } from "@/types";
import { ElementRef, useCallback, useMemo, useRef, useState } from "react";

export const UserSearchForm = () => {
  const [searchQuery, setSearchQuery] = useState("");
  const [users, setUsers] = useState<User[]>([]);

  const { candidates, setCandidates } = useCandidates();

  const searchFormRef = useRef<ElementRef<"div">>(null);
  const inputRef = useRef<ElementRef<"input">>(null);

  const callApi = useCallback(
    debounce(async (query: string) => {
      try {
        if (query.length === 0) {
          setUsers([]);
          return;
        }

        const data = await fetchUsersData(query);

        setUsers(
          data.map((user) => ({
            ...user,
            workHistory: user.workHistory.sort(
              (a, b) =>
                new Date(b.startDate).getTime() -
                new Date(a.startDate).getTime()
            ),
          }))
        );
      } catch (error) {
        console.log(error);
      }
    }, 150),
    []
  );

  const filteredUsers = useMemo(
    () =>
      users.filter((user) =>
        candidates.every(
          (candidate) =>
            `${candidate.firstName} ${candidate.lastName}` !==
            `${user.firstName} ${user.lastName}`
        )
      ),
    [users, candidates]
  );

  const handleChange = useCallback(
    (value: string) => {
      setSearchQuery(value);

      const newValue = value.trim();

      callApi(newValue);
    },
    [callApi]
  );

  const handleUserClick = useCallback(
    (user: User) => {
      const fullName = `${user.firstName} ${user.lastName}`;

      setCandidates((prev) => [...prev, user]);
      setUsers((prev) =>
        prev.filter((item) => `${item.firstName} ${item.lastName}` !== fullName)
      );
      setSearchQuery("");

      inputRef.current?.focus();
    },
    [setCandidates]
  );

  return (
    <div ref={searchFormRef} className="relative mx-5 my-[18px] group">
      <Input
        value={searchQuery}
        onChange={handleChange}
        placeholder="Michael Jordan..."
        inputRef={inputRef}
      />

      {filteredUsers.length !== 0 && (
        <ul className="hidden group-focus-within:block py-0.5 absolute mt-1 w-full border border-gray-300 bg-white rounded divide-y divide-gray-300">
          {filteredUsers.map((user) => (
            <li key={user.firstName} className="flex">
              <button
                onClick={() => handleUserClick(user)}
                className="w-full px-2.5 py-1.5 transition-colors hover:bg-gray-200 duration-300 text-xs font-medium text-left focus-visible:outline-none focus-visible:bg-gray-200"
              >
                {user.firstName} {user.lastName}
              </button>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};
