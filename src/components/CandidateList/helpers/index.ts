import { User } from "@/types";

const YEARS_IN_MS = 1000 * 60 * 60 * 24 * 365;

/**
 * Calculates the total work experience based on the user's work history.
 *
 * @param workHistory - Array of objects representing the user's work history.
 * @returns The total work experience in years.
 */
export const calculateTotalWorkExperience = (workHistory: User['workHistory']): number => {
  const currentDate = new Date();
  let totalExperience = 0;

  for (const job of workHistory) {
    const jobStartDate = new Date(job.startDate);
    // In case user still working at the company
    const jobEndDate = job.endDate ? new Date(job.endDate) : currentDate;

    const jobExperience = (jobEndDate.getTime() - jobStartDate.getTime()) / YEARS_IN_MS;
    
    totalExperience += jobExperience;
  }

  return Math.floor(totalExperience);
}

/**
 * Calculates the work experience for each job in the user's work history.
 *
 * @param job - The job object containing the start and end dates.
 * @returns The work experience in years and months.
 */
export const calculateEachWorkExperience = (job: User['workHistory'][number]) => {
  const startDate = new Date(job.startDate);
  // In case user still working at the company
  const endDate = job.endDate ? new Date(job.endDate) : new Date();

  // Calculate the total months of experience
  const monthsOfExperience = (endDate.getFullYear() - startDate.getFullYear()) * 12 + (endDate.getMonth() - startDate.getMonth());

  // Calculate the number of years and remainder in months
  const yearsOfExperience = Math.floor(monthsOfExperience / 12);
  const monthsOfExperienceRemainder = monthsOfExperience % 12;

  if (yearsOfExperience === 0) {
    return `${monthsOfExperienceRemainder} mos`;
  } else {
    if (monthsOfExperienceRemainder === 0) {
      return `${yearsOfExperience} yrs`;
    } else {
      return `${yearsOfExperience} yrs ${monthsOfExperienceRemainder} mos`;
    }
  }
}
