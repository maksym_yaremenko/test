import CandidateImage from "@/assets/candidate_image.png";
import CrossIcon from "@/assets/cross.svg";
import LocationIcon from "@/assets/location.svg";
import RandomCompanyLogo from "@/assets/random_company_logo.png";
import { useCandidates } from "@/store";
import { User } from "@/types";
import Image from "next/image";
import { useCallback } from "react";
import {
  calculateEachWorkExperience,
  calculateTotalWorkExperience,
} from "./helpers";

export const CandidateList = () => {
  const { candidates, setCandidates } = useCandidates();

  const handleCandidateRemove = useCallback(
    (candidate: User) => {
      const fullName = `${candidate.firstName} ${candidate.lastName}`;

      setCandidates((prev) =>
        prev.filter((item) => `${item.firstName} ${item.lastName}` !== fullName)
      );
    },
    [setCandidates]
  );

  if (candidates.length === 0) {
    return (
      <p className="text-center p-10 text-gray-500 text-3xl m-auto font-medium">
        No candidates
      </p>
    );
  }

  return (
    <ul className="flex flex-col gap-3.5 p-5 border-t border-gray-300">
      {candidates.map((candidate) => (
        <li
          key={candidate.firstName}
          className="flex flex-col border border-gray-300 rounded-lg transition-all duration-500 hover:shadow-xl"
        >
          <div className="p-4 flex justify-end border-b border-gray-300">
            <button onClick={() => handleCandidateRemove(candidate)}>
              <Image
                src={CrossIcon}
                alt={`Remove ${candidate.firstName} ${candidate.lastName} from list`}
              />
            </button>
          </div>

          <div className="flex gap-3 px-4 py-3">
            <div className="flex flex-1 gap-2.5 items-center">
              <Image
                width={62}
                height={62}
                className="border border-gray-300 rounded-md"
                src={CandidateImage}
                alt={`${candidate.firstName} ${candidate.lastName} avatar`}
              />

              <div className="flex flex-col gap-1.5">
                <p className="text-xl font-bold text-gray-900">
                  {candidate.firstName} {candidate.lastName}
                </p>

                <div className="flex">
                  <Image src={LocationIcon} alt="location icon" />

                  <p className="text-sm font-medium text-gray-500">
                    {candidate.location}
                  </p>
                </div>
              </div>
            </div>

            <div className="flex flex-col gap-0.5 items-center px-3 py-2 border border-gray-300 bg-gradient-light-brown rounded">
              <p className="text-xs font-semibold text-gray-900">Experience</p>

              <p className="align-baseline w-24 text-xs text-center">
                <span className="mr-1 text-xl font-black text-gray-900">
                  {calculateTotalWorkExperience(candidate.workHistory)}
                </span>
                years
              </p>
            </div>
          </div>

          {candidate.workHistory.length !== 0 && (
            <div className="flex flex-col gap-2.5 px-4 pb-4">
              <div className="flex gap-2 items-center">
                <p className="text-xs font-semibold text-gray-500">
                  Work History · {candidate.workHistory.length}
                </p>

                <div className="flex-1 h-px bg-gray-300" />
              </div>

              <ul className="flex flex-col gap-1.5">
                {candidate.workHistory.map((item) => (
                  <li key={item.company + item.title} className="flex gap-2">
                    <i className="p-px rounded-sm h-max bg-gray-100">
                      <Image
                        height={18}
                        width={18}
                        className="rounded-sm"
                        src={RandomCompanyLogo}
                        alt={`${item.company} logo`}
                      />
                    </i>

                    <div className="flex gap-1 items-center">
                      <p className="text-sm font-medium text-gray-900">
                        {item.company}
                      </p>

                      <p className="text-sm font-bold text-gray-500">·</p>

                      <p className="text-sm font-medium text-gray-900">
                        {item.title}
                      </p>

                      <p className="text-xs font-semibold text-gray-500">
                        {calculateEachWorkExperience(item)}
                      </p>
                    </div>
                  </li>
                ))}
              </ul>
            </div>
          )}
        </li>
      ))}
    </ul>
  );
};
