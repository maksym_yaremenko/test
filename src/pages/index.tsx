import { CandidateList } from "@/components/CandidateList";
import { Header } from "@/components/Header";
import { UserSearchForm } from "@/components/UserSearch";

export default function Home() {
  return (
    <main className="bg-white flex flex-col min-h-screen">
      <Header />

      <div className="border flex-1 border-gray-300">
        <UserSearchForm />

        <CandidateList />
      </div>
    </main>
  );
}
