import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/UI/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        'gradient-light-brown': 'linear-gradient(91.09deg, #FEFDF7 21.48%, #F8FBFA 83.61%);'
      },
      colors: {
        gray: {
          100: "#E1E7EB",
          200: '#F1F3F5',
          300: "#DFE5E9",
          500: "#7E899C",
          900: "#10151B",
        },
        accent: {
          600: '#324DFF',
        }
      },
    }
  },
  plugins: [],
};
export default config;
